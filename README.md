# html-template
Basic file structure for creating an HTML site

# Getting Started

1. Install Node packages: "npm install"
2. Generate css & js files:
    - "gulp css-app"
    - "gulp css-vendor"
    - "gulp js-vendor"
3. Start the development server: "npm start"