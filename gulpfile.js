var gulp = require('gulp');
var less = require('gulp-less');
var cleanCss = require('gulp-clean-css');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var util = require('gulp-util');
var closure = require('google-closure-compiler').gulp();

var appCssFiles = ['./styles/less/theme.less'];

function swallowError (error) {
    util.log(error.message);
    this.emit('end');
};

// CSS - App Tasks
gulp.task('css-app', function(){
    return gulp.src(appCssFiles)
        .pipe(plumber(swallowError))
        .pipe(less())
        .pipe(cleanCss())
        .pipe(concat('app.css'))
        .pipe(gulp.dest('./styles/'));
});

gulp.task('css-app-watch', function(){
    return watch(appCssFiles, function(){
        gulp.start(['css-app']);
    });
});

// CSS - Vendor Tasks
gulp.task('css-vendor', ['iconfonts'], function(){
    return gulp.src(['./node_modules/bootstrap/dist/css/bootstrap.min.css',
                     './node_modules/font-awesome/css/font-awesome.min.css'])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./styles/'));
});

gulp.task('iconfonts', function(){
    return gulp.src(['./node_modules/font-awesome/fonts/*',
                     './node_modules/bootstrap/dist/fonts/*'])
        .pipe(gulp.dest('./fonts/'));
});

// JS - Vendor Tasks
gulp.task('js-vendor', function(){
    return closure({
        js: ['./node_modules/jquery/dist/jquery.min.js',
             './node_modules/bootstrap/dist/js/bootstrap.min.js'],
        compilation_level: 'WHITESPACE_ONLY',
        js_output_file: 'vendor.min.js'
    }).src()
    .pipe(gulp.dest('./scripts/'));    
});